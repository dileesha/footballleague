/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.dilee.footballmanager.model;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author DIleesha Rajapakse
 */
public abstract class SportsClub implements Serializable{
    
    private String clubId;
    private String clubName;
    private String address;
    private Date startedDate;
    private int noOfMembers;
    private String chairman;

    /**
     * @return the clubId
     */
    public String getClubId() {
        return clubId;
    }

    /**
     * @param clubId the clubId to set
     */
    public void setClubId(String clubId) {
        this.clubId = clubId;
    }

    /**
     * @return the clubName
     */
    public String getClubName() {
        return clubName;
    }

    /**
     * @param clubName the clubName to set
     */
    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the startedDate
     */
    public Date getStartedDate() {
        return startedDate;
    }

    /**
     * @param startedDate the startedDate to set
     */
    public void setStartedDate(Date startedDate) {
        this.startedDate = startedDate;
    }

    /**
     * @return the noOfMembers
     */
    public int getNoOfMembers() {
        return noOfMembers;
    }

    /**
     * @param noOfMembers the noOfMembers to set
     */
    public void setNoOfMembers(int noOfMembers) {
        this.noOfMembers = noOfMembers;
    }

    /**
     * @return the chairman
     */
    public String getChairman() {
        return chairman;
    }

    /**
     * @param chairman the chairman to set
     */
    public void setChairman(String chairman) {
        this.chairman = chairman;
    }
    
}
