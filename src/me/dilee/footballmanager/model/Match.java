/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.dilee.footballmanager.model;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author DIleesha Rajapakse
 */
public class Match implements Serializable{
    
    private String matchId;
    private String matchStatus;
    private String[][] result;
    private int team01Goals;
    private int team02Goals;
    private String location;
    private Date date;
    private String bestPlayer;

    /**
     * @return the matchId
     */
    public String getMatchId() {
        return matchId;
    }

    /**
     * @param matchId the matchId to set
     */
    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    /**
     * @return the matchStatus
     */
    public String getMatchStatus() {
        return matchStatus;
    }

    /**
     * @param matchStatus the matchStatus to set
     */
    public void setMatchStatus(String matchStatus) {
        this.matchStatus = matchStatus;
    }

    /**
     * @return the result
     */
    public String[][] getResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(String[][] result) {
        this.result = result;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the bestPlayer
     */
    public String getBestPlayer() {
        return bestPlayer;
    }

    /**
     * @param bestPlayer the bestPlayer to set
     */
    public void setBestPlayer(String bestPlayer) {
        this.bestPlayer = bestPlayer;
    }

    /**
     * @return the team01Goals
     */
    public int getTeam01Goals() {
        return team01Goals;
    }

    /**
     * @param team01Goals the team01Goals to set
     */
    public void setTeam01Goals(int team01Goals) {
        this.team01Goals = team01Goals;
    }

    /**
     * @return the team02Goals
     */
    public int getTeam02Goals() {
        return team02Goals;
    }

    /**
     * @param team02Goals the team02Goals to set
     */
    public void setTeam02Goals(int team02Goals) {
        this.team02Goals = team02Goals;
    }


    
}
