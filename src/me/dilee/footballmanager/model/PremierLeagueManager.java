/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.dilee.footballmanager.model;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Scanner;
import me.dilee.footballmanager.controller.DMLayer;

/**
 *
 * @author DIleesha Rajapakse
 */
public class PremierLeagueManager implements LeagueManager {

    public final DMLayer dml = new DMLayer();

    @Override
    public boolean addClub() {
        FootballClub club = new FootballClub();
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter Club Name : ");
        club.setClubName(sc.nextLine());
        System.out.print("Enter Club Address : ");
        club.setAddress(sc.nextLine());
        System.out.print("Enter Club Started Date (yyyy-MM-dd) : ");
        club.setStartedDate(java.sql.Date.valueOf(sc.nextLine().trim()));
        System.out.print("Enter Current Number of Members : ");
        club.setNoOfMembers(sc.nextInt());
        assignUniqueClubId(club);

        int result = dml.saveClub(club);

        if (result > 0) {
            System.out.println("Club Added Successfully!");
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean relegateClub() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Club ID to relegate (ex: chelsea-123) : ");
        String clubId = sc.nextLine().trim().toLowerCase();
        boolean success = false;

        if (dml.searchObject("club", clubId) != null) {
            System.out.print("Are you sure? (y/n): ");
            if (sc.next().toLowerCase().equals("y")) {
                success = dml.removeClub(clubId);
            }
        } else {
            return success;
        }

        if (success) {
            System.out.println("Successfully Deleted!");
        }

        return success;
    }

    @Override
    public boolean addMatch() {
        Match match = new Match();
        Scanner sc = new Scanner(System.in);
        String result[][] = new String[2][2];
        int team1Goals = 0;
        int team2Goals = 0;

        System.out.println("1 - Played Match");
        System.out.println("2 - Schedule Match");
        System.out.print("Choice : ");
        int choice = sc.nextInt();
        sc.nextLine();
        if (choice == 1) {
            System.out.print("Enter Location : ");
            match.setLocation(sc.nextLine());
            System.out.print("Enter Match Date (yyyy-MM-dd) : ");
            match.setDate(Date.valueOf(sc.nextLine().trim()));
            System.out.print("Enter Team 01 ID (ex: chelsea-123) : ");
            result[0][0] = sc.nextLine();

            if (dml.searchObject("club", result[0][0]) == null) {
                System.out.println("Club doesn't exist!");
                return false;
            } else {
                System.out.print("Enter Goals by Team 01 : ");
                team1Goals = sc.nextInt();
                sc.nextLine();
                result[0][1] = team1Goals + "";
            }

            System.out.print("Enter Team 02 ID (ex: chelsea-123) : ");
            result[1][0] = sc.nextLine();

            if (dml.searchObject("club", result[1][0]) == null) {
                System.out.println("Club doesn't exist!");
                return false;
            } else {
                System.out.print("Enter Goals by Team 02 : ");
                team2Goals = sc.nextInt();
                sc.nextLine();
                result[1][1] = team2Goals + "";
            }

            match.setResult(result);

            if (result[0][1].equals(result[1][1])) {
                match.setMatchStatus("Drawn");
            } else {
                if (team1Goals > team2Goals) {
                    match.setMatchStatus(result[0][0]);
                } else {
                    match.setMatchStatus(result[1][0]);
                }
            }

            dml.updateTeams(result, team1Goals, team2Goals);

            System.out.print("Player of the Match (Player ID) : ");
            match.setBestPlayer(sc.nextLine());

        } else if (choice == 2) {
            System.out.print("Enter Location : ");
            match.setLocation(sc.nextLine());
            System.out.print("Enter Match Date (yyyy-MM-dd) : ");
            match.setDate(Date.valueOf(sc.nextLine().trim()));
            System.out.print("Enter Team 01 ID (ex: chelsea-123) : ");
            result[0][0] = sc.nextLine();

            if (dml.searchObject("club", result[0][0]) == null) {
                System.out.println("Club doesn't exist!");
                return false;
            }

            System.out.print("Enter Team 02 ID (ex: chelsea-123) : ");
            result[1][0] = sc.nextLine();
            result[0][1] = "";
            result[1][1] = "";

            if (dml.searchObject("club", result[1][0]) == null) {
                System.out.println("Club doesn't exist!");
                return false;
            }
            match.setMatchStatus("To be Played");
            match.setResult(result);
        }

        assignUniqueMatchId(match);

        match.setTeam01Goals(team1Goals);
        match.setTeam02Goals(team2Goals);

        int totalGoals = team1Goals + team2Goals;

        if (totalGoals != 0) {
            addPlayerStats(match, totalGoals);
        }
        int response = dml.saveMatch(match);

        if (response > 0) {
            System.out.println("Match Added Successfully!");
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void viewPointsTable() {

        ArrayList<FootballClub> list = dml.getAllClubs();
        ArrayList<FootballClub> listTemp = null;

        if (list != null) {

            sortClubs(list);

            int counter = 1;
            System.out.println("");
            System.out.println("****** Points Table ******");
            for (FootballClub club : list) {
                System.out.println("");
                System.out.println("Position : " + counter);
                System.out.println("Club Name : " + club.getClubName());
                System.out.println("Club ID : " + club.getClubId());
                System.out.println("Points : " + club.getPoints());
                System.out.println("Wins : " + club.getWins());
                System.out.println("Defeats : " + club.getDefeats());
                System.out.println("Draws : " + club.getDraws());
                System.out.println("Total Goals Scored : " + club.getTotalGoalsScored());
                System.out.println("Total Goals Recieved : " + club.getTotalGoalsReceived());
                System.out.println("Total Matches Played : " + club.getMatchesPlayed());
                System.out.println("");
                counter++;
            }
        } else {
            System.out.println("No Club Found!");
            return;
        }
        System.out.println("**************************");
        System.out.println("");

    }

    public boolean addPlayer() {
        Scanner sc = new Scanner(System.in);
        Player player = new Player();

        System.out.print("Enter Player Name : ");
        player.setName(sc.nextLine());
        System.out.print("Enter Date of Birth (yyyy-MM-dd) : ");
        player.setdOB(java.sql.Date.valueOf(sc.nextLine().trim()));
        System.out.print("Club ID (ex: chelsea-123) : ");
        String clubId = sc.nextLine().trim();

        if (dml.searchObject("club", clubId) != null) {
            player.setClubId(clubId);
        } else {
            System.out.println("Club not found!");
            return false;
        }

        System.out.print("Enter Height (centimetres) : ");
        player.setHeight(sc.nextDouble());
        System.out.print("Enter Weight (kilograms) : ");
        player.setWeight(sc.nextDouble());

        assignUniquePlayerId(player);

        int response = dml.savePlayer(player);

        if (response > 0) {
            System.out.println("Player Added Successfully!");
            return true;
        } else {
            return false;
        }

    }

    public void viewPlayer() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Player ID (ex: rooney-34) : ");
        String playerId = sc.nextLine();

        Player player = (Player) dml.retrievePlayer(playerId);

        if (player != null) {
            System.out.println("Player Name : " + player.getName());
            System.out.println("Player Club ID : " + player.getClubId());
            System.out.println("Date of Birth : " + player.getdOB());
            System.out.println("Height (cm) : " + player.getHeight());
            System.out.println("Weight (kg) : " + player.getWeight());
            System.out.println("Goals Scored : " + player.getGoalsScored());
        } else {
            System.out.println("Player ID doesn't match any of the records!");
            return;
        }

    }

    public void viewClub() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Club ID (ex: chelsea-123) : ");
        String clubId = sc.nextLine();
        ArrayList<String> playerlist = new ArrayList<String>();
        FootballClub club = (FootballClub) dml.retrieveClub(clubId);

        if (club != null) {
            System.out.println("Club Name : " + club.getClubName());
            System.out.println("Points : " + club.getPoints());
            System.out.println("Wins : " + club.getWins());
            System.out.println("Defeats : " + club.getDefeats());
            System.out.println("Draws : " + club.getDraws());
            System.out.println("Total Goals Scored : " + club.getTotalGoalsScored());
            System.out.println("Total Goals Recieved : " + club.getTotalGoalsReceived());
            System.out.println("Total Matches Played : " + club.getMatchesPlayed());

            playerlist = dml.retrieveClubPlayers(clubId);

            if (!playerlist.isEmpty()) {
                int counter = 1;
                for (String pId : playerlist) {
                    System.out.println("");
                    System.out.println("Players List :");
                    System.out.println("\t" + counter + ". " + pId);
                }
            }

        } else {
            System.out.println("Club ID doesn't match any of the records!");
            return;
        }
    }

    public void addPlayerStats(Match match, int totalGoals) {
        Scanner sc = new Scanner(System.in);
        for (int i = 1; i <= totalGoals; i++) {
            System.out.println("Goal " + i + " scored by (player ID) : ");
            String playerId = sc.nextLine();
            if (dml.searchObject("player", playerId) != null) {
                incrementPlayerGoals(playerId);
            } else {
                System.out.println("Player does not exist!");
                break;
            }
        }
    }

    private void incrementPlayerGoals(String playerId) {
        Player player = (Player) dml.searchObject("player", playerId);
        int currentGoals = player.getGoalsScored();

        if (player != null) {
            player.setGoalsScored(++currentGoals);
            dml.updatePlayer(player);
        } else {
            System.out.println("Player not found!");
            return;
        }
    }

    private void assignUniqueClubId(FootballClub club) {
        int clubId = (int) (Math.random() * 100);
        FootballClub tempClub = (FootballClub) (dml.searchObject("club", (club.getClubName().toLowerCase().split(" ")[0] + "-" + clubId + "")));

        if (tempClub != null || clubId == 0) {
            assignUniqueClubId(club);
        } else {
            club.setClubId(club.getClubName().toLowerCase().split(" ")[0] + "-" + clubId);
        }
    }

    private void assignUniquePlayerId(Player player) {
        int playerId = (int) (Math.random() * 100);
        Player tempPlayer = (Player) (dml.searchObject("player", (player.getName().toLowerCase().split(" ")[0] + "-" + playerId + "")));

        if (tempPlayer != null || playerId == 0) {
            assignUniquePlayerId(player);
        } else {
            player.setPlayerId(player.getName().toLowerCase().split(" ")[0] + "-" + playerId);
        }
    }

    private void assignUniqueMatchId(Match match) {
        int matchId = (int) (Math.random() * 100);
        Player tempPlayer = (Player) (dml.searchObject("matches", matchId + ""));

        if (tempPlayer != null || matchId == 0) {
            assignUniqueMatchId(match);
        } else {
            match.setMatchId(matchId + "");
        }
    }

    public boolean removePlayer() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Player ID to relegate (ex: player-00) : ");
        String playerId = sc.nextLine().trim().toLowerCase();
        boolean success = false;

        if (dml.searchObject("player", playerId) != null) {
            System.out.print("Are you sure? (y/n): ");
            if (sc.next().toLowerCase().equals("y")) {
                success = dml.removeClub(playerId);
            }
        } else {
            System.out.println("Player not found!");
            return success;
        }
        return success;
    }

    public void viewCalender() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("\tEnter a Year : ");
        int year = scanner.nextInt();

        System.out.print("\tEnter a Month (ex : 02) : ");
        int month = scanner.nextInt();

        Calendar cal = new GregorianCalendar();

        // set its date to the first day of the month/year given by user
        cal.clear();
        cal.set(year, month - 1, 1);

        // print calendar header
        System.out.println("\n"
                + cal.getDisplayName(Calendar.MONTH, Calendar.LONG,
                        Locale.US) + " " + cal.get(Calendar.YEAR));

        // obtain the weekday of the first day of month.
        int weekOne = cal.get(Calendar.DAY_OF_WEEK);

        // obtain the number of days in month.
        int daysInMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

        // print anonymous calendar month based on the weekday of the first
        // day of the month and the number of days in month:
        int weekdayIndex = 0;

        // print calendar weekday header
        System.out.println("Su  Mo  Tu  We  Th  Fr  Sa");

        // leave/skip weekdays before the first day of month
        for (int day = 1; day < weekOne; day++) {
            System.out.print("    ");
            weekdayIndex++;
        }

        // print the days of month in tabular format.
        for (int day = 1; day <= daysInMonth; day++) {
            // print day
            System.out.printf("%1$2d", day);

            // next weekday
            weekdayIndex++;
            // if it is the last weekday
            if (weekdayIndex == 7) {
                // reset it
                weekdayIndex = 0;
                // and go to next line
                System.out.println();
            } else { // otherwise
                // print space
                System.out.print("  ");
            }
        }

        // print a final new-line.
        System.out.println();

        System.out.println("");
        System.out.println("1 - Search match by Date");
        System.out.println("2 - Return to main menu");

        Scanner sc = new Scanner(System.in);

        if (sc.nextInt() == 1) {
            viewFixtures();
        } else {
            return;
        }

    }

    private void viewFixtures() {
        Scanner sc = new Scanner(System.in);
        System.out.println("**** Search Matches by Date ****");
        System.out.print("Enter a Date (yyyy-MM-dd) : ");

        ArrayList<Match> list = dml.getMatches(java.sql.Date.valueOf(sc.nextLine()));

        if (list != null) {
            for (Match match : list) {
                System.out.println("");
                String status = match.getMatchStatus();
                System.out.println("Match ID : " + match.getMatchId());
                System.out.println("Match Status : " + status);

                if (status.toLowerCase().equals("to be played")) {
                    System.out.println("Team 01 : " + match.getResult()[0][0]);
                    System.out.println("Team 01 : " + match.getResult()[1][0]);
                    System.out.println("Venue : " + match.getLocation());
                    System.out.println("");
                } else {
                    System.out.println("Venue : " + match.getLocation());
                    System.out.print("Team 01 : " + match.getResult()[0][0]);
                    System.out.println("\tGoals : " + match.getTeam01Goals());
                    System.out.print("Team 02 : " + match.getResult()[1][0]);
                    System.out.println("\tGoals : " + match.getTeam02Goals());
                    System.out.println("Best Player : " + match.getBestPlayer());
                    System.out.println("");

                }
            }
        } else {
            System.out.println("No match has been scheduled!");
            return;
        }
    }

    public void viewAllMatches() {
        ArrayList<Match> matches = dml.getAllMatches();

        for (Match match : matches) {
            System.out.println("");
            String status = match.getMatchStatus();
            System.out.println("Date : " + match.getDate());
            System.out.println("Match ID : " + match.getMatchId());
            System.out.println("Match Status : " + status);

            if (status.toLowerCase().equals("to be played")) {
                System.out.println("Team 01 : " + match.getResult()[0][0]);
                System.out.println("Team 01 : " + match.getResult()[1][0]);
                System.out.println("Venue : " + match.getLocation());
                System.out.println("");
            } else {
                System.out.println("Venue : " + match.getLocation());
                System.out.print("Team 01 : " + match.getResult()[0][0]);
                System.out.println("\tGoals : " + match.getTeam01Goals());
                System.out.print("Team 02 : " + match.getResult()[1][0]);
                System.out.println("\tGoals : " + match.getTeam02Goals());
                System.out.println("Best Player : " + match.getBestPlayer());
                System.out.println("");

            }
        }
    }

    private ArrayList<FootballClub> sortClubs(ArrayList<FootballClub> list) {

        FootballClub tempClub = null;
        FootballClub club1 = null;
        FootballClub club2 = null;
        ArrayList<FootballClub> tempList = new ArrayList<FootballClub>();
        int club1GD = 0;
        int club2GD = 0;

        for (int i = 0; i < list.size(); i++) {

            for (int j = i + 1; j < list.size(); j++) {
                club1 = list.get(i);
                club2 = list.get(j);

                if (club1.getPoints() == club2.getPoints()) {

                    club1GD = club1.getTotalGoalsScored() - club1.getTotalGoalsReceived();
                    club1GD = club2.getTotalGoalsScored() - club2.getTotalGoalsReceived();

                    if (club1GD < club2GD) {
                        tempClub = club1;
                        list.set(i, club2);
                        list.set(j, tempClub);
                    }

                } else if (club1.getPoints() < club2.getPoints()) {
                    tempClub = club1;
                    list.set(i, club2);
                    list.set(j, tempClub);
                }
            }

        }
        
        return tempList;
    }

}
