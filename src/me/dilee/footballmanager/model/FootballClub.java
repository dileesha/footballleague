/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.dilee.footballmanager.model;

import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * @author DIleesha Rajapakse
 */
public class FootballClub extends SportsClub implements Serializable {

    private int wins;
    private int defeats;
    private int draws;
    private int totalGoalsScored;
    private int totalGoalsReceived;
    private int matchesPlayed;
    private int points;

    /**
     * @return the wins
     */
    public int getWins() {
        return wins;
    }

    /**
     * @param wins the wins to set
     */
    public void setWins(int wins) {
        this.wins = wins;
    }

    /**
     * @return the defeats
     */
    public int getDefeats() {
        return defeats;
    }

    /**
     * @param defeats the defeats to set
     */
    public void setDefeats(int defeats) {
        this.defeats = defeats;
    }

    /**
     * @return the draws
     */
    public int getDraws() {
        return draws;
    }

    /**
     * @param draws the draws to set
     */
    public void setDraws(int draws) {
        this.draws = draws;
    }

    /**
     * @return the totalGoalsScored
     */
    public int getTotalGoalsScored() {
        return totalGoalsScored;
    }

    /**
     * @param totalGoalsScored the totalGoalsScored to set
     */
    public void setTotalGoalsScored(int totalGoalsScored) {
        this.totalGoalsScored = totalGoalsScored;
    }

    /**
     * @return the totalGoalsReceived
     */
    public int getTotalGoalsReceived() {
        return totalGoalsReceived;
    }

    /**
     * @param totalGoalsReceived the totalGoalsReceived to set
     */
    public void setTotalGoalsReceived(int totalGoalsReceived) {
        this.totalGoalsReceived = totalGoalsReceived;
    }

    /**
     * @return the matchesPlayed
     */
    public int getMatchesPlayed() {
        return matchesPlayed;
    }

    /**
     * @param matchesPlayed the matchesPlayed to set
     */
    public void setMatchesPlayed(int matchesPlayed) {
        this.matchesPlayed = matchesPlayed;
    }

    /**
     * @return the points
     */
    public int getPoints() {
        return points;
    }

    /**
     * @param points the points to set
     */
    public void setPoints(int points) {
        this.points = points;
    }

}
