/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.dilee.footballmanager.model;

/**
 *
 * @author DIleesha Rajapakse
 */
public interface LeagueManager  {
    
    boolean addClub();
    boolean relegateClub();
    boolean addMatch();
    void viewPointsTable();
    
}
