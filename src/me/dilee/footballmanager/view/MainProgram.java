/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.dilee.footballmanager.view;

import java.awt.BorderLayout;
import java.util.Scanner;
import me.dilee.footballmanager.controller.DBConnection;
import me.dilee.footballmanager.controller.DMLayer;
import me.dilee.footballmanager.model.FootballClub;
import me.dilee.footballmanager.model.Match;
import me.dilee.footballmanager.model.Player;
import me.dilee.footballmanager.model.PremierLeagueManager;

/**
 *
 * @author DIleesha Rajapakse
 */
public class MainProgram {

    public static final DMLayer dml = new DMLayer();
    public static PremierLeagueManager pml = new PremierLeagueManager();

    public static void main(String[] args) throws Exception {

        while (true) {
            initMenu();
        }

    }

    public static void initMenu() {
        System.out.println("");
        System.out.println("**************** EuropaCup Premierleague ****************");
        System.out.println("");
        System.out.println("1 - Add New Club");
        System.out.println("2 - Relegate Club");
        System.out.println("3 - Add Player");
        System.out.println("4 - Remove Player");
        System.out.println("5 - View Club Details");
        System.out.println("6 - View Player Details");
        System.out.println("7 - Add Match");
        System.out.println("8 - View Points Table");
        System.out.println("9 - View Calender and fixtures");
        System.out.println("10 - View match list");
        System.out.println("11 - Exit");
        System.out.println("");
        System.out.print("Enter the corresponding number : ");
        Scanner sc = new Scanner(System.in);
        int input = Integer.parseInt(sc.nextLine());

        switch (input) {
            case 1:
                pml.addClub();
                break;
            case 2:
                pml.relegateClub();
                break;
            case 3:
                pml.addPlayer();
                break;
            case 4:
                pml.removePlayer();
                break;
            case 5:
                pml.viewClub();
                break;
            case 6:
                pml.viewPlayer();
                break;
            case 7:
                pml.addMatch();
                break;
            case 8:
                pml.viewPointsTable();
                break;
            case 9:
                pml.viewCalender();
                break;
            case 10:
                pml.viewAllMatches();
                break;
            case 11:
                System.exit(0);
                break;

        }

    }

}
