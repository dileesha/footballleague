/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.dilee.footballmanager.controller;

import com.mysql.jdbc.Statement;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import me.dilee.footballmanager.model.FootballClub;
import me.dilee.footballmanager.model.Match;
import me.dilee.footballmanager.model.Player;

/**
 * This class contains all the general data management methods
 *
 * @author Dilee
 */
public class DMLayer {

    private static final DBConnection dbCon = DBConnection.getInstance();
    private static final Connection connection = DBConnection.getConnection();

    /**
     * this method serialises and saves the club to the database
     *
     * @param club club to be saved
     * @return auto generated ID
     * @throws SQLException
     */
    public int saveClub(FootballClub club) {

        String sql = "INSERT INTO club(clubId, clubObject) VALUES (?,?)";
        int recordId = -1;

        try {
            PreparedStatement pstmt = connection
                    .prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            pstmt.setObject(1, club.getClubId());
            pstmt.setObject(2, club);
            pstmt.executeUpdate();
            ResultSet rs = pstmt.getGeneratedKeys();

            if (rs.next()) {
                recordId = rs.getInt(1);
            }
            rs.close();
            pstmt.close();

        } catch (Exception e) {
            System.out.println("Something went wrong, program is shutting down...");
            e.printStackTrace();
        }

        System.out.println("Club ID : " + club.getClubId());

        return recordId;
    }

    /**
     * this method retrieves the serialised club from the database
     *
     * @param clubId club to be retrieved
     * @return club object
     */
    public Object retrieveClub(String clubId) {
        Object rmObj = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "select * from club where clubId='" + clubId + "'";

        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();

            if (rs.next()) {
                ByteArrayInputStream bais;
                ObjectInputStream ins;
                bais = new ByteArrayInputStream(rs.getBytes("clubObject"));
                ins = new ObjectInputStream(bais);
                rmObj = ins.readObject();
                ins.close();
            }

        } catch (Exception e) {
            System.out.println("Something went wrong, program is shutting down...");
            e.printStackTrace();
        }
        return rmObj;
    }

    /**
     * this method serialises and saves the player to the database
     *
     * @param player player to be saved
     * @return auto generated ID
     * @throws SQLException
     */
    public int savePlayer(Player player) {

        String sql = "INSERT INTO player(playerId, clubId, playerObject) VALUES (?, ?, ?)";
        int recordId = -1;

        try {
            PreparedStatement pstmt = connection
                    .prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            pstmt.setObject(1, player.getPlayerId());
            pstmt.setObject(2, player.getClubId());
            pstmt.setObject(3, player);
            pstmt.executeUpdate();
            ResultSet rs = pstmt.getGeneratedKeys();

            if (rs.next()) {
                recordId = rs.getInt(1);
            }
            rs.close();
            pstmt.close();

        } catch (Exception e) {
            System.out.println("Something went wrong, program is shutting down...");
            e.printStackTrace();
        }

        System.out.println("Player ID : " + player.getPlayerId());

        return recordId;
    }

    /**
     * this method retrieves the serialised player from the database
     *
     * @param playerId player to be retrieved
     * @return player object
     */
    public Object retrievePlayer(String playerId) {
        Object rmObj = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "select * from player where playerId='" + playerId + "'";

        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();

            if (rs.next()) {
                ByteArrayInputStream bais;
                ObjectInputStream ins;
                bais = new ByteArrayInputStream(rs.getBytes("playerObject"));
                ins = new ObjectInputStream(bais);
                rmObj = ins.readObject();
                ins.close();
            }

        } catch (Exception e) {
            System.out.println("Something went wrong, program is shutting down...");
            e.printStackTrace();
        }
        return rmObj;
    }

    public boolean removeClub(String clubId) {

        java.sql.Statement stmt = null;
        ResultSet rs = null;
        String sql = "DELETE FROM club WHERE clubId ='" + clubId.toLowerCase() + "'";
        int success = 0;

        try {
            stmt = connection.createStatement();
            success = stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println("Something went wrong, program is shutting down...");
            e.printStackTrace();
        }
        if (success == 1) {
            return true;
        } else {
            return false;
        }

    }

    public boolean updatePlayer(Player player) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "UPDATE player SET playerObject=? WHERE playerId=?";
        int success = 0;

        try {
            ps = connection.prepareStatement(sql);
            ps.setObject(1, player);
            ps.setString(2, player.getPlayerId());
            success = ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            System.out.println("Something went wrong, program is shutting down...");
            e.printStackTrace();
        }
        if (success == 1) {
            return true;
        } else {
            return false;
        }
    }

    public boolean removePlayer(String playerId) {

        java.sql.Statement stmt = null;
        ResultSet rs = null;
        String sql = "DELETE FROM player WHERE playerId ='" + playerId.toLowerCase() + "'";
        int success = 0;

        try {
            stmt = connection.createStatement();
            success = stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println("Something went wrong, program is shutting down...");
            e.printStackTrace();
        }
        if (success == 1) {
            System.out.println("Successfully deleted the Player!");
            return true;
        } else {
            return false;
        }

    }

    /**
     * this method serialises and saves the match to the database
     *
     * @param match match to be saved
     * @return auto generated ID
     * @throws SQLException
     */
    public int saveMatch(Match match) {

        String sql = "INSERT INTO matches(matchesId, matchDate, matchObject) VALUES (?,?,?)";
        int recordId = -1;

        try {
            PreparedStatement pstmt = connection
                    .prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            pstmt.setObject(1, match.getMatchId());
            pstmt.setDate(2, match.getDate());
            pstmt.setObject(3, match);
            pstmt.executeUpdate();
            ResultSet rs = pstmt.getGeneratedKeys();

            if (rs.next()) {
                recordId = rs.getInt(1);
            }
            rs.close();
            pstmt.close();

        } catch (Exception e) {
            System.out.println("Something went wrong, program is shutting down...");
            e.printStackTrace();
        }

        //System.out.println("Club ID : " + club.getClubId());
        return recordId;
    }

    /**
     * this method retrieves the serialised match from the database
     *
     * @param matchId match to be retrieved
     * @return match object
     */
    public Object retrieveMatch(int matchId) {

        Object rmObj = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "select * from match where id=" + matchId;

        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();

            if (rs.next()) {
                ByteArrayInputStream bais;
                ObjectInputStream ins;
                bais = new ByteArrayInputStream(rs.getBytes("matchObject"));
                ins = new ObjectInputStream(bais);
                rmObj = ins.readObject();
                ins.close();
            }

        } catch (Exception e) {
            System.out.println("Something went wrong, program is shutting down...");
            e.printStackTrace();
        }
        return rmObj;
    }

    public Object searchObject(String tableName, String searchId) {
        Object rmObj = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "select * from " + tableName + " where " + tableName + "Id='" + searchId.toLowerCase() + "'";

        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();

            if (rs.next()) {
                ByteArrayInputStream bais;
                ObjectInputStream ins;
                bais = new ByteArrayInputStream(rs.getBytes(tableName + "Object"));
                ins = new ObjectInputStream(bais);
                rmObj = ins.readObject();
                ins.close();
            }

        } catch (Exception e) {
            System.out.println("Something went wrong, program is shutting down...");
            e.printStackTrace();
        }
        return rmObj;
    }

    public void updateTeams(String[][] teams, int team01G, int team02G) {

        if (team01G == team02G) {
            updateTeamStats(teams[0][0], team01G, team02G, 2);
            updateTeamStats(teams[1][0], team02G, team01G, 2);
        } else if (team01G > team02G) {
            updateTeamStats(teams[0][0], team01G, team02G, 4);
            updateTeamStats(teams[1][0], team02G, team01G, 1);
        } else if (team02G > team02G) {
            updateTeamStats(teams[0][0], team01G, team02G, 1);
            updateTeamStats(teams[1][0], team02G, team01G, 4);
        }

    }

    private boolean updateTeamStats(String teamId, int goalsScored, int goalsRecieved, int points) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "UPDATE club SET clubObject=? WHERE clubId=?";
        FootballClub team = (FootballClub) searchObject("club", teamId);

        team.setTotalGoalsScored(team.getTotalGoalsScored() + goalsScored);
        team.setTotalGoalsReceived(team.getTotalGoalsReceived() + goalsRecieved);
        team.setPoints(team.getPoints() + points);
        team.setMatchesPlayed(team.getMatchesPlayed() + 1);
        
        if(points == 2){
            team.setDraws(team.getDraws()+1);
        } else if(points == 1){
            team.setDefeats(team.getDefeats()+1);
        } else if(points == 4){
            team.setWins(team.getWins()+1);
        }

        int success = 0;

        try {
            ps = connection.prepareStatement(sql);
            ps.setObject(1, team);
            ps.setString(2, team.getClubId());
            success = ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            System.out.println("Something went wrong, program is shutting down...");
            e.printStackTrace();
        }
        if (success == 1) {
            return true;
        } else {
            return false;
        }
    }

    public ArrayList getMatches(Date date) {

        Object rmObj = null;
        java.sql.Statement stmt = null;
        ResultSet rs = null;
        ArrayList<Match> list = new ArrayList<Match>();
        Match match = null;
        String sql = "select * from matches where matchDate='" + date + "'";

        try {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ByteArrayInputStream bais;
                ObjectInputStream ins;
                bais = new ByteArrayInputStream(rs.getBytes("matchObject"));
                ins = new ObjectInputStream(bais);
                rmObj = ins.readObject();
                match = (Match) rmObj;
                list.add(match);
                ins.close();
            }

        } catch (Exception e) {
            System.out.println("Something went wrong, program is shutting down...");
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList getAllMatches() {

        Object rmObj = null;
        java.sql.Statement stmt = null;
        ResultSet rs = null;
        ArrayList<Match> list = new ArrayList<Match>();
        Match match = null;
        String sql = "select * from matches order by matchDate asc";

        try {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ByteArrayInputStream bais;
                ObjectInputStream ins;
                bais = new ByteArrayInputStream(rs.getBytes("matchObject"));
                ins = new ObjectInputStream(bais);
                rmObj = ins.readObject();
                match = (Match) rmObj;
                list.add(match);
                ins.close();
            }

        } catch (Exception e) {
            System.out.println("Something went wrong, program is shutting down...");
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList getAllClubs() {

        Object rmObj = null;
        java.sql.Statement stmt = null;
        ResultSet rs = null;
        ArrayList<FootballClub> list = new ArrayList<FootballClub>();
        FootballClub club = null;
        String sql = "select * from club";

        try {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ByteArrayInputStream bais;
                ObjectInputStream ins;
                bais = new ByteArrayInputStream(rs.getBytes("clubObject"));
                ins = new ObjectInputStream(bais);
                rmObj = ins.readObject();
                club = (FootballClub) rmObj;
                list.add(club);
                ins.close();
            }

        } catch (Exception e) {
            System.out.println("Something went wrong, program is shutting down...");
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<String> retrieveClubPlayers(String clubId) {
        Object rmObj = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Player player = null;
        ArrayList<String> list = new ArrayList<String>();
        String sql = "select * from player where clubId='" + clubId + "'";

        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();

            if (rs.next()) {
                String str = rs.getNString("playerId");
                list.add(str);
            }

        } catch (Exception e) {
            System.out.println("Something went wrong, program is shutting down...");
            e.printStackTrace();
        }
        return list;
    }

}
