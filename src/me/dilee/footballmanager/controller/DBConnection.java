/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.dilee.footballmanager.controller;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;

/**
 *
 * @author DIleesha Rajapakse
 */
public class DBConnection {
 
    private static java.sql.Connection connection;
    private static final DBConnection dbCon = new DBConnection();
    
    private DBConnection(){
        // connecting to the database
        try {
            Class.forName("com.mysql.jdbc.Driver");
            //Customer Details Store connection string, username and password

            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/premierleague", "root", "melamine");
            System.out.println("Successfully connected to the Database...");

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static DBConnection getInstance(){
        return dbCon;
    }
    
    public static java.sql.Connection getConnection(){
        return connection;
    }
    
}
